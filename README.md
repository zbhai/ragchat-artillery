# RAGChat-Artillery

# Purposes
This repository provides load tests for the [RAGChat-API](https://gitlab.rrz.uni-hamburg.de/zbhai/ragchat-api).
Therefor it's making use of the globally installed artillery package

# Prerequisits
- [npm](https://www.npmjs.com/) installed
- [RAGChat-API](https://gitlab.rrz.uni-hamburg.de/zbhai/ragchat-api) running & reachable
- To prevent CORS errors add the frontend address to backends .env FRONTEND_URL

# Install
```
git clone https://gitlab.rrz.uni-hamburg.de/zbhai/ragchat-artillery.git
cd ragchat-artillery
npm i
cp ./.env.template ./.env
# fill envs with production and/or devel values
# evtl. edit tags on package.json => scripts => start
npm start
```


# Sources
- [RAGChat-API](https://gitlab.rrz.uni-hamburg.de/zbhai/ragchat-api)
- [PM2](https://pm2.keymetrics.io/)

# Roadmap
