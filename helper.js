const { faker } = require('@faker-js/faker');

module.exports = {
  createUser
};

function createUser(userContext, events, done) {
  userContext.vars.name = faker.person.fullName();
  userContext.vars.username = `artillery_${faker.internet.userName()}`;
  userContext.vars.email = faker.internet.email();
  userContext.vars.password = faker.internet.password({ length: 16, prefix: '#8aZ_' });
  return done();
};